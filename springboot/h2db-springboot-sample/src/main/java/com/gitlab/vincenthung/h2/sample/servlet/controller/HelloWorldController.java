package com.gitlab.vincenthung.h2.sample.servlet.controller;

import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class HelloWorldController {

	@RequestMapping("/")
	public String index() {
		return "Hello World";
	}

}
