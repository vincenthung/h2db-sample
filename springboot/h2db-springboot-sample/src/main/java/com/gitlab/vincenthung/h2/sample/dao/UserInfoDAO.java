package com.gitlab.vincenthung.h2.sample.dao;

import com.gitlab.vincenthung.h2.sample.entity.UserInfo;
import com.googlecode.genericdao.dao.hibernate.GenericDAO;

public interface UserInfoDAO extends GenericDAO<UserInfo, Long> {

	public UserInfo findUser(Long id);

	public UserInfo createUser(String userName, String gender, Short age);

	public boolean updateUser(UserInfo userInfo);

	public boolean deleteUser(Long id);

}
