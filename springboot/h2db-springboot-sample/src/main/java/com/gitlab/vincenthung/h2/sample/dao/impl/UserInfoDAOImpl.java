package com.gitlab.vincenthung.h2.sample.dao.impl;

import java.util.Date;

import org.springframework.transaction.annotation.Transactional;

import com.gitlab.vincenthung.h2.sample.dao.UserInfoDAO;
import com.gitlab.vincenthung.h2.sample.entity.UserInfo;
import com.googlecode.genericdao.dao.hibernate.GenericDAOImpl;
import com.googlecode.genericdao.search.Search;

@Transactional(transactionManager = "transactionManager", readOnly = true)
public class UserInfoDAOImpl extends GenericDAOImpl<UserInfo, Long> implements UserInfoDAO {


	public UserInfo findUser(Long id) {
		Search search = new Search();
		search.addFilterEqual("id", id);
		return searchUnique(search);
	}

	@Transactional(readOnly = false)
	public UserInfo createUser(String userName, String gender, Short age) {

		UserInfo userInfo = new UserInfo();
		userInfo.setUserName(userName);
		userInfo.setGender(gender);
		userInfo.setAge(age);

		userInfo.setCreateDatetime(new Date());

		save(userInfo);

		return userInfo;

	}

	@Transactional(readOnly = false)
	public boolean updateUser(UserInfo userInfo) {
		save(userInfo);
		return true;
	}

	@Transactional(readOnly = false)
	public boolean deleteUser(Long id) {
		UserInfo userInfo = findUser(id);
		if (userInfo != null)
			return remove(userInfo);
		return false;
	}

}
