package com.gitlab.vincenthung.h2.sample.configuration.properties;

import org.apache.log4j.Logger;

public class DataSourceProperties {

	private static final Logger logger = Logger.getLogger(DataSourceProperties.class);

	private String host;
	private String port;
	private String database;

	private String username;
	private String password;

	private static final String PROP_KEY_HOST		= "embedded.h2.conn.host";
	private static final String PROP_KEY_PORT		= "embedded.h2.conn.port";
	private static final String PROP_KEY_DATABASE	= "embedded.h2.conn.database";
	private static final String PROP_KEY_USERNAME	= "embedded.h2.conn.username";
	private static final String PROP_KEY_PASSWORD	= "embedded.h2.conn.password";

	public static DataSourceProperties collect() {
		DataSourceProperties dbProps = new DataSourceProperties();

		dbProps.host = System.getProperty(PROP_KEY_HOST, "localhost");
		dbProps.port = System.getProperty(
				PROP_KEY_PORT, String.valueOf(H2DatabaseConfigurationProperties.DEFAULT_TCP_PORT));
		dbProps.database = System.getProperty(PROP_KEY_DATABASE, "./test");
		dbProps.username = System.getProperty(PROP_KEY_USERNAME, "sa");
		dbProps.password = System.getProperty(PROP_KEY_PASSWORD, "P@ssw0rd");

		logger.info(dbProps);

		return dbProps;
	}

	public String getHost() {
		return host;
	}

	public void setHost(String host) {
		this.host = host;
	}

	public String getPort() {
		return port;
	}

	public void setPort(String port) {
		this.port = port;
	}

	public String getDatabase() {
		return database;
	}

	public void setDatabase(String database) {
		this.database = database;
	}

	public String getUsername() {
		return username;
	}

	public void setUsername(String username) {
		this.username = username;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	@Override
	public String toString() {
		return "DataSourceProperties [host=" + host + ", port=" + port + ", database=" + database + ", username="
				+ username + ", password=" + password + "]";
	}

}
