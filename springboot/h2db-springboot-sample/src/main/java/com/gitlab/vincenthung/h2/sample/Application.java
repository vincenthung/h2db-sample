package com.gitlab.vincenthung.h2.sample;

import java.io.File;
import java.io.InputStream;
import java.util.Properties;

import org.apache.commons.io.FileUtils;
import org.apache.commons.io.FilenameUtils;
import org.apache.commons.lang3.StringUtils;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.ExitCodeGenerator;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.orm.jpa.HibernateJpaAutoConfiguration;
import org.springframework.boot.builder.SpringApplicationBuilder;
import org.springframework.boot.context.properties.EnableConfigurationProperties;
import org.springframework.boot.web.support.SpringBootServletInitializer;
import org.springframework.context.annotation.ImportResource;

@SpringBootApplication(exclude = HibernateJpaAutoConfiguration.class)
@EnableConfigurationProperties
@ImportResource("classpath:/spring/applicationContext*.xml")
public class Application extends SpringBootServletInitializer implements CommandLineRunner {

	@Override
	protected SpringApplicationBuilder configure(SpringApplicationBuilder application) {
		return application.sources(Application.class);
	}

	@Override
	public void run(String... arg0) throws Exception {
		if (arg0.length > 0 && arg0[0].equals("exitcode")) {
			throw new ExitException();
		}
	}

	public static void main(String[] args) throws Exception {

		String baseDir = System.getProperty("baseDir");

		if (StringUtils.isBlank(baseDir)) {
			baseDir = new File(Application.class.getProtectionDomain().getCodeSource().getLocation()
				    .toURI()).getParent();
			System.getProperties().put("baseDir", baseDir);
		}

		/* Load Embedded H2 Properties for Embedded Tomcat */
		Properties props = new Properties();

		InputStream is = FileUtils.openInputStream(new File(
				FilenameUtils.concat(baseDir, "conf/embedded-h2.properties")));
		props.load(is);

		System.getProperties().putAll(props);

		new SpringApplication(Application.class).run(args);
	}

	class ExitException extends RuntimeException implements ExitCodeGenerator {
		private static final long serialVersionUID = 1L;

		@Override
		public int getExitCode() {
			return 10;
		}

	}
}
