package com.gitlab.vincenthung.h2.sample.servlet.controller;

import java.text.MessageFormat;

import javax.annotation.Resource;

import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.gitlab.vincenthung.h2.sample.dao.UserInfoDAO;
import com.gitlab.vincenthung.h2.sample.entity.UserInfo;

@RestController
public class UserInfoManagerController {

	@Resource
	private UserInfoDAO userInfoDAO;

	@RequestMapping("/create/{name}")
	public String createUser(
			@PathVariable String name,
			@RequestParam(required = false) String gender,
			@RequestParam(required = false) Short age) {

		userInfoDAO.createUser(name, gender, age);

		return MessageFormat.format("Name: {0}, Gender: {1}, Age: {2}", name, gender, age);
	}

	@RequestMapping("/delete/{id}")
	public String deleteUser(@PathVariable Long id) {

		boolean removed = userInfoDAO.deleteUser(id);

		return MessageFormat.format("deleted: {0}", removed);
	}

	@RequestMapping("/update/{id}")
	public String deleteUser(@PathVariable Long id,
			@RequestParam(required = false) String name,
			@RequestParam(required = false) String gender,
			@RequestParam(required = false) Short age) {

		UserInfo userInfo = userInfoDAO.findUser(id);
		if (userInfo == null)
			return "User not found";

		if (name != null)
			userInfo.setUserName(name);
		if (gender != null)
			userInfo.setGender(gender);
		if (age != null)
			userInfo.setAge(age);

		if (userInfoDAO.updateUser(userInfo))
			return "Updated user";
		else
			return "Failed updating user";

	}
}
