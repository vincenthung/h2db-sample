package com.gitlab.vincenthung.h2.sample.configuration.properties;

import java.util.ArrayList;
import java.util.List;

import org.apache.commons.lang3.StringUtils;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.PropertySource;
import org.springframework.stereotype.Component;

@Component(value = "h2Props")
@PropertySource("file:${baseDir}/conf/embedded-h2.properties")
@ConfigurationProperties(prefix = "embedded.h2.server" )
public class H2DatabaseConfigurationProperties {

	public static final int DEFAULT_TCP_PORT = 9092;

	private boolean enabled			= true;

	private String	baseDir			= "./h2/db";
	private int		tcpPort			= DEFAULT_TCP_PORT;
	private String	tcpPassword		= null;
	private boolean	tcpAllowOthers	= true;
	private boolean tcpSSL			= false;
	private boolean tcpDaemon		= false;
	private boolean ifExists		= false;
	private boolean	trace 			= false;
	private String	key				= null;
	private String	keyDatabase		= null;

	public String[] constructArgs() {
		List<String> args = new ArrayList<String>();

		args.add("-tcpPort");
		args.add(String.valueOf(tcpPort > 0 ? tcpPort : DEFAULT_TCP_PORT));

		if (StringUtils.isNotBlank(baseDir)) {
			args.add("-baseDir");
			args.add(baseDir);
		}

		if (StringUtils.isNotBlank(tcpPassword)) {
			args.add("-tcpPassword");
			args.add(tcpPassword);
		}

		if (tcpAllowOthers)
			args.add("-tcpAllowOthers");

		if (tcpSSL)
			args.add("-tcpSSL");

		if (tcpDaemon)
			args.add("-tcpDaemon");

		if (ifExists)
			args.add("-ifExists");
		else
			args.add("-ifNotExists");

		if (trace)
			args.add("-trace");

		if (StringUtils.isNotBlank(key) && StringUtils.isNotBlank(keyDatabase)) {
			args.add("-key");
			args.add(key);
			args.add(keyDatabase);
		}

		return args.toArray(new String[] {});
	}

	public boolean isEnabled() {
		return enabled;
	}

	public void setEnabled(boolean enabled) {
		this.enabled = enabled;
	}

	public String getBaseDir() {
		return baseDir;
	}

	public void setBaseDir(String baseDir) {
		this.baseDir = baseDir;
	}

	public int getTcpPort() {
		return tcpPort;
	}

	public void setTcpPort(int tcpPort) {
		this.tcpPort = tcpPort;
	}

	public String getTcpPassword() {
		return tcpPassword;
	}

	public void setTcpPassword(String tcpPassword) {
		this.tcpPassword = tcpPassword;
	}

	public boolean isTcpAllowOthers() {
		return tcpAllowOthers;
	}

	public void setTcpAllowOthers(boolean tcpAllowOthers) {
		this.tcpAllowOthers = tcpAllowOthers;
	}

	public boolean isTcpSSL() {
		return tcpSSL;
	}

	public void setTcpSSL(boolean tcpSSL) {
		this.tcpSSL = tcpSSL;
	}

	public boolean isTcpDaemon() {
		return tcpDaemon;
	}

	public void setTcpDaemon(boolean tcpDaemon) {
		this.tcpDaemon = tcpDaemon;
	}

	public boolean isIfExists() {
		return ifExists;
	}

	public void setIfExists(boolean ifExists) {
		this.ifExists = ifExists;
	}

	public boolean isTrace() {
		return trace;
	}

	public void setTrace(boolean trace) {
		this.trace = trace;
	}

	public String getKey() {
		return key;
	}

	public void setKey(String key) {
		this.key = key;
	}

	public String getKeyDatabase() {
		return keyDatabase;
	}

	public void setKeyDatabase(String keyDatabase) {
		this.keyDatabase = keyDatabase;
	}

	@Override
	public String toString() {
		return "H2DatabaseConfigurationProperties [enabled=" + enabled + ", baseDir=" + baseDir + ", tcpPort=" + tcpPort
				+ ", tcpPassword=" + tcpPassword + ", tcpAllowOthers=" + tcpAllowOthers + ", tcpSSL=" + tcpSSL
				+ ", tcpDaemon=" + tcpDaemon + ", ifExists=" + ifExists + ", trace=" + trace + ", key=" + key
				+ ", keyDatabase=" + keyDatabase + "]";
	}

}
