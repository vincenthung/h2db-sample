package com.gitlab.vincenthung.h2.sample.configuration;

import java.text.MessageFormat;

import org.apache.catalina.Context;
import org.apache.catalina.startup.Tomcat;
import org.apache.tomcat.util.descriptor.web.ContextResource;
import org.apache.tomcat.util.descriptor.web.ContextResourceLink;
import org.springframework.boot.context.embedded.tomcat.TomcatEmbeddedServletContainer;
import org.springframework.boot.context.embedded.tomcat.TomcatEmbeddedServletContainerFactory;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import com.gitlab.vincenthung.h2.sample.configuration.properties.DataSourceProperties;

@Configuration
public class TomcatConfiguration {

	@Bean
	public TomcatEmbeddedServletContainerFactory tomcatFactory() {

		return new TomcatEmbeddedServletContainerFactory() {

			private String constructH2DatabaseUrl(String host, String port, String database) {
				return MessageFormat.format("jdbc:h2:tcp://{0}:{1}/{2};", host, port, database);
			}

			@Override
			protected TomcatEmbeddedServletContainer getTomcatEmbeddedServletContainer(
					Tomcat tomcat) {
				tomcat.enableNaming();
				return super.getTomcatEmbeddedServletContainer(tomcat);
			}

			@Override
			protected void postProcessContext(Context context) {

				DataSourceProperties dbProps = DataSourceProperties.collect();

				ContextResource resource = new ContextResource();
				resource.setName("jdbc/H2DB");
				resource.setType("javax.sql.DataSource");
				resource.setProperty("driverClassName", "org.h2.Driver");
				resource.setProperty("url", constructH2DatabaseUrl(
						dbProps.getHost(), dbProps.getPort(), dbProps.getDatabase()));
				resource.setProperty("username", dbProps.getUsername());
				resource.setProperty("password", dbProps.getPassword());
				resource.setProperty("auth", "Container");

				context.getNamingResources().addResource(resource);

				ContextResourceLink link = new ContextResourceLink();
				link.setName("jdbc/H2DB");
				link.setGlobal("jdbc/H2DB");
				link.setType(javax.sql.DataSource.class.getName());
				link.setProperty("auth", "Container");

				context.getNamingResources().addResourceLink(link);
			}

		};
	}

}
