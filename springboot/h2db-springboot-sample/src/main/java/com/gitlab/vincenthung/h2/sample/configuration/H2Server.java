package com.gitlab.vincenthung.h2.sample.configuration;

import java.sql.SQLException;
import java.text.MessageFormat;

import org.apache.log4j.Logger;
import org.h2.server.Service;
import org.h2.server.TcpServer;
import org.h2.tools.Server;

import com.gitlab.vincenthung.h2.sample.configuration.properties.H2DatabaseConfigurationProperties;

public class H2Server extends Server {

	public H2Server() {}

	public H2Server(Service service, String... args) throws SQLException {
        super(service, args);
    }

	private static final Logger logger = Logger.getLogger(H2Server.class);

	private static H2DatabaseConfigurationProperties h2Props;

    public static H2Server createTcpServer() throws SQLException {

    	logger.info(MessageFormat.format(
    			"Using H2 Database Server Configuration: {0}",
    			h2Props));

    	TcpServer service = new TcpServer();
    	H2Server server = new H2Server(service, h2Props.constructArgs());
        service.setShutdownHandler(server);
        return server;
    }

    public static void setH2Props(H2DatabaseConfigurationProperties h2Props) {
    	H2Server.h2Props = h2Props;
    }

    @Override
    public Server start() throws SQLException {
    	return h2Props.isEnabled() ? super.start() : null;
    }

}
